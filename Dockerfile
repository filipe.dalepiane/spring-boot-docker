FROM openjdk:17-jdk-alpine
# implement user no root
RUN addgroup -S spring && adduser -S spring -G spring
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]

# docker build --build-arg JAR_FILE=build/libs\*.jar -t springio/spring-boot-docker .
# docker run -p 8080:8080 springio/spring-boot-docker